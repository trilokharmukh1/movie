const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


//Q1. Find all the movies with total earnings more than $500M. 
function totalEarning(favouritesMovies, earningMoney) {

    let result = Object.entries(favouritesMovies).reduce((acc, movie) => {
        earningMoney = earningMoney.replace('$', "").replace('M', "");
        let totalEarning = movie[1].totalEarnings.replace('$', "").replace('M', "");
        if (totalEarning > earningMoney) {
            acc[movie[0]] = movie[1];
        }
        return acc;
    }, {})

    return result;

}



//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
function filterByOscarNomination(favouritesMovies, oscarNominateTime, earningMoney) {
    if (typeof (earningMoney) == 'string') {
        earningMoney = earningMoney.replace('$', "").replace('M', "");
    }

    let result = Object.entries(favouritesMovies).reduce((acc, movie) => {
        let totalEarning = movie[1].totalEarnings.replace('$', "").replace('M', "");
        if (totalEarning > earningMoney && movie[1].oscarNominations > oscarNominateTime) {
            acc[movie[0]] = movie[1];
        }
        return acc;
    }, {})

    return result;

}


//Q.3 Find all movies of the actor "Leonardo Dicaprio".
function actorsMovie(favouritesMovies, actorName) {
    let result = Object.entries(favouritesMovies).reduce((acc, movie) => {
        if (movie[1].actors.includes(actorName)) {
            acc[movie[0]] = movie[1];
        }
        return acc;
    }, {})
    return result;
}



//Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.
function sortedByImdb(favouritesMovies) {
    let result = Object.entries(favouritesMovies).sort((movieA, movieB) => {
        if (movieB[1].imdbRating == movieA[1].imdbRating) {
            let earningA = movieA[1].totalEarnings.replace('$', "").replace('M', "");
            let earningB = movieB[1].totalEarnings.replace('$', "").replace('M', "");
            return earningB - earningA;
        } else {
            return movieB[1].imdbRating - movieA[1].imdbRating;
        }
    })
    return result;
}

